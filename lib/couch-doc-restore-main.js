#!/usr/bin/env node
"use strict";
var log = require("util-logging")
  , util = require("util")
  , readline = require("readline")
  , cradle = require("cradle")
  , async = require("async")
  , _ = require("lodash")
  , fs = require("fs");

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var str2Range = function (str, max) {
  if (!str) return [];

  var indices = {};
  var elems = str.split(",");

  elems.forEach(function (elem) {
    var range = elem.split("-");
    var start_value, end_value;
    if (range.length > 2 || range.length <= 0) return;
    if (range.length == 1) {
      start_value = parseInt(range[0]);
      if (start_value <= max) indices[start_value] = true;
    }

    start_value = parseInt(range[0]);
    end_value = parseInt(range[1]);

    if (start_value <= 0 || start_value > max) return;
    if (end_value <= 0 || end_value > max) return;
    if (start_value > end_value) return;
    for (var i = start_value; i <= end_value; i++) {
      indices[i] = true;
    }
  });

  var indices_array = Object.keys(indices);
  indices_array.sort(function (a, b) {
    var iA = parseInt(a);
    var iB = parseInt(b);
    if (iA < iB) return -1;
    if (iA > iB) return 1;
    return 0;
  });

  return indices_array;
};

var range2Str = function (range) {
  if (range.length == 0) {
    return "";
  }

  var result = new String(range[0]);
  var prev = parseInt(range[0]);
  var count = 1;

  for (var i = 1; i < range.length; i++) {
    var val = parseInt(range[i]);

    var diff = val - prev;
    if (diff == 1) {
      count++;
      prev = val;
      continue;
    }

    if (diff > 1) {
      if (count > 1) {
        result = result + "-" + new String(prev) + "," + new String(val);
        prev = val;
        count = 1;
        continue;
      }

      result = result + "," + new String(val);
      prev = val;
      continue;
    }
  }

  if (count > 1) {
    result = result + "-" + new String(range[range.length - 1]);
  }

  return result;
};

var mergeDocs = function (allDocs, range) {
  var pickedDocs = [{}];
  for (var i = 0; i < range.length; i++) {
    var val = parseInt(range[i]);
    var doc = allDocs[val - 1];
    pickedDocs.push(doc);
  }
  var merged = _.assign.apply(this, pickedDocs);
  return merged;
};

module.exports = function (argv) {
  var logger = new log.ConsoleLogger().setLevel(log.Level.INFO);

  cradle.setup({
    host: argv['host'],
    cache: false
  });

  var couch = new (cradle.Connection)().database(argv["db"]);

  var done = function (err) {
    if (err) {
      console.error(err);
    }

    rl.close();
    console.log("Done");
  };

  var allDocs = [];
  var _revs_info = [];
  couch.get(argv["doc"], {"revs_info": true}, function (err, res) {
    if (err) return done(err);

    _revs_info = res._revs_info;
    var loopCount = 0;

    rl.question(util.format("\nThere are %s revisions in %s. Continue to fetch them? (Y/n): ", _revs_info.length, argv["doc"]), function (answer) {
      if ((answer && (answer.indexOf("n") == 0 || answer.indexOf("N") == 0))) {
        console.log("Okay, well, goodbye ...");
        return done();
      }
      async.eachLimit(
        _revs_info,
        200,
        function (_rev_info, revDone) {
          if (_rev_info.status !== "available") {
            return revDone();
          }

          couch.get(argv["doc"], _rev_info.rev, function (err, doc) {
            console.log('Downloaded ' + (++loopCount) + "/" + _revs_info.length + " ... \r");
            if (err) return revDone(err);
            allDocs.push(doc);
            return revDone();
          });
        },
        function (err) {
          if (err) return done(err);
          allDocs.sort(function (a, b) {
            if (a.updatedAt_ > b.updatedAt_) return 1;
            if (a.updatedAt_ < b.updatedAt_) return -1;
            return 0;
          });

          allDocs.forEach(function (doc, index) {
            var date = new Date(doc.updatedAt_).toISOString();
            var count = Object.keys(doc).length;
            var _rev = doc._rev;
            console.log((index + 1) + ". " + "_rev: " + _rev + ", date: " + date + ", count: " + count);
          });

          console.log("Now you are going to choose the range of values for the merge operation.");
          console.log("//Example 1");
          console.log("//Choose individual values values 1, 10 and 400");
          console.log("1, 10, 400");
          console.log("");
          console.log("//Example 2");
          console.log("//Choose a range of values between 35 and 144")
          console.log("35-144");
          console.log("");
          console.log("//Example 3");
          console.log("//Combine individual values, and ranges");
          console.log("1,10,35-144,400");

          var rangeStr = "";
          var range = [];

          var range_selection_done = false;
          async.doWhilst(
            function (cb1) {
              async.doWhilst(
                function (cb) {
                  rl.question(util.format("\nEnter documents range: "), function (answer) {
                    rangeStr = answer;
                    return cb();
                  });
                },
                function () {
                  range = str2Range(rangeStr, allDocs.length);
                  if (range.length == 0) {
                    console.log("The range entered is not valid. Try again.");
                    return true;
                  }
                  return false;
                },
                function (err) {
                  if (err) return done(err);

                  console.log("The effective range is: " + range2Str(range));

                  rl.question(util.format("\nDo you want to change it? (y/N): "), function (answer) {
                    range_selection_done = !(answer && (answer.indexOf("y") == 0 || answer.indexOf("Y") == 0));
                    return cb1();
                  });
                }
              );
            },
            function () {
              return !range_selection_done;
            },
            function (err) {
              if (err) return done(err);

              var merged_doc = mergeDocs(allDocs, range);

              console.log("Merged document contains " + Object.keys(merged_doc).length + " keys(s). It has been written to \"merged_document.json\", where you can preview it.");
              fs.writeFileSync("merged_document.json", JSON.stringify(merged_doc, null, 2));
              rl.question(util.format("\nSave it to the database? (Y/n): "), function (answer) {
                if (answer && (answer.indexOf("n") == 0 || answer.indexOf("N") == 0)) {
                  return done();
                }

                couch.get(argv["doc"], function (err, doc) {
                  if (err) return done(err);
                  merged_doc._rev = doc._rev;
                  couch.save(merged_doc.id, merged_doc, function (err) {
                    if (err) return done(err);
                    console.log("Merged document has been saved.");
                    return done(err);
                  })
                });
              });
            }
          );
        }
      );
    });
  });
};

module.exports.usage =
  "\n" +
  "Usage:\n couch-doc-restore  --host=<string> --db=<string> --doc=<string> \n";


module.exports.options = {
  "host": {
    describe: "CouchDB hostname, or IP",
    required: true,
    type: "string"
  },
  "db": {
    describe: "CouchDB database name",
    required: true,
    type: "string"
  },
  "doc": {
    describe: "CouchDB document name",
    required: true,
    type: "string"
  }
};
