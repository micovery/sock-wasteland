[TOC]

# sock-wasteland#

  * This is a set of Node.js scripts that can be used for managing Arma 3 Wasteland servers that use [sock-rpc-stats Node.js module](https://www.npmjs.org/package/sock-rpc-stats) for persistence.
  * The mission source code, and released PBO binaries are available over at the forked [ArmA3_Wasteland.Altis git repository](https://github.com/micovery/ArmA3_Wasteland.Altis)

## Changelog ##
  * 0.0.3 - Add ```couch-doc-restore``` script
  * 0.0.2 - Add ```server-boot-players``` script
  * 0.0.1 - Initial release

## Prerequisites ##
  * Download and install [Node.js](http://nodejs.org/download/)


## Installation ##
  * From the head of the master branch  
  .  
  ```
npm install -g git+https://bitbucket.org/micovery/sock-wasteland.git
   ```  
.    

  * From the npmjs.org repository  
.  
  ``` 
npm install -g sock-wasteland  
  ```  
.    



##server-restart-hint##
  This script does not really restart the server. It simply talks to the server, and lets it know that a restart is coming up.  
  
  When that happens, the server does some final cleanup activities such as saving all the vehicles, and objects that are active in the map.  
  Also, the server notifies all players that are currently connected to the server, that a restart is coming up, and coerces them to report their stats.
  
  The script waits for the server's acknowledgement that it has completed the tasks, for up to ```--wait``` number of seconds.

```
Options:
  --host      host, IP address of the stats server to connect to   [required]
  --port      port number of the stats server to connect to        [required]
  --wait      total number of seconds to wait for server response  [default: 120]
  --name      prefix name of the server to communicate with        [default: "A3W"]
```

##server-boot-players##
  This script boots all the players to the lobby

  The script waits for the server's acknowledgement that it has completed the tasks, for up to ```--wait``` number of seconds.

```
Options:
  --host      host, IP address of the stats server to connect to   [required]
  --port      port number of the stats server to connect to        [required]
  --wait      total number of seconds to wait for server response  [default: 120]
  --name      prefix name of the server to communicate with        [default: "A3W"]
```

  
  
##inidb-migrate##
  This script is used for migrating an existing iniDB database to the [sock-rpc-stats](https://www.npmjs.org/package/sock-rpc-stats) system.  
   
  It simply navigates through all the ini files on a user-specified directory, and loads the data into a running [sock-rpc-stats](https://www.npmjs.org/package/sock-rpc-stats) server.  
  
  The logic is as follows:  

  * The name of each ini file becomes a new **scope** in the stats server  
  * Each section inside an ini file becomes a **key** in within the respective scope  
  * variables inside a section become nested keys  
  
  
  The rules for parsing variable values are as follows (in order of evaluation):

  * If the variable name is in the ```--ignored``` list, then ignore it
  * If the variable name is in the ```--strings``` list, then coerce it into a JSON String
  * If the variable name is in the ```--numbers``` list, then coerce it into a JSON Number, or set the value to 0 
  * If the variable name is in the ```--booleans``` list, then coerce it into a JSON Boolean   (false values are: ```"false"```, ```"0"```, ```""```, ```"null"```, ```"nil"```)
  * If it matches ```"-1.#IND"```,  ```"-1.#INF"```, or  ```NaN```  is treated s JSON Number 0
  * If it matches ```"nil"```, value is set to JSON null
  * If the value cab be parsed using JSON.parse, the use the parsed value
  * Otherwise, coerce the value into a JSON String

```
Options:
  --host      IP address of the stats server to connect to                     [default: "localhost"]
  --port      port number of the stats server to connect to                    [default: 1337]
  --path      path to the directory that contains the database ini files       [required]
  --prefix    prefix value for filtering the list of ini files (e.g. "A3W_")  [default: ""]
  --strings   list of variable names that should be coerced into JSON String   [default: "ownerUID,UID"]
  --numbers   list of variable names that should be coerced into JSON Number   [default: ""]
  --booleans  list of variable names that should be coerced into JSON Boolean  [default: ""]
  --ignored   list of variable names that should be ignored                    [default: ""]
  --trace     trace the JSON content of the parsed ini files                   [default: false]
```




##couch-doc-restore##
  This script allows administrator to restore a CouchDB document from its previous revisions.
  Using the script the admin can choose a set of revisions to merge and use the merged contents to replace the
  most recent revision.


```
Options:
  --host      CouchDB hostname, or IP                        [string] [required]
  --db        CouchDB database name                          [string] [required]
  --doc       CouchDB document name                          [string] [required]
  --help, -h  Show help                                                [boolean]
```
